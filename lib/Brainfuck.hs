{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Brainfuck (BF(..), evalBF) where

import Control.Monad.State
import Control.Monad.Writer
import Data.Char (chr, ord)
import Data.Foldable (sequenceA_, traverse_)
import Data.Text.Lazy (Text)
import qualified Data.Text.Lazy as T
import Data.Vector (Vector, (!), (//))
import qualified Data.Vector as V

data BF =
    MoveRight
  | MoveLeft
  | Increment
  | Decrement
  | Put
  | Get
  | Conditional [BF]
  deriving (Show)

evalBF :: [BF] -> IO ((), Text, Int, Vector Int)
evalBF = runBF . traverse_ dispatch

dispatch :: BF -> Brainfuck ()
dispatch bf =
  case bf of
    MoveRight -> moveRight
    MoveLeft -> moveLeft
    Increment -> increment
    Decrement -> decrement
    Put -> put'
    Get -> get'
    Conditional bfs -> conditional $ dispatch <$> bfs

data Context = Context
  { pointer :: Int
  , tape :: Vector Int }

newtype Brainfuck a = Brainfuck (WriterT Text (StateT Context IO) a)
  deriving (Functor, Applicative, Monad, MonadIO, MonadState Context, MonadWriter Text)

runBF :: Brainfuck a -> IO (a, Text, Int, Vector Int)
runBF (Brainfuck bf) = f <$> i
  where
  s = runWriterT bf
  i = runStateT s $ Context 0 (V.replicate 100 0)
  f ((x, t), (Context p v)) = (x, t, p, v)

moveRight :: Brainfuck ()
moveRight = do
  c <- get
  let p = pointer c + 1
  put c { pointer = p }

moveLeft :: Brainfuck ()
moveLeft = do
  c <- get
  let p = pointer c - 1
  put c { pointer = p }

increment :: Brainfuck ()
increment = do
  Context p v <- get
  let x = v ! p
      v' = v // [(p, x + 1)]
  put $ Context p v'

decrement :: Brainfuck ()
decrement = do
  Context p v <- get
  let x = v ! p
      v' = v // [(p, x - 1)]
  put $ Context p v'

put' :: Brainfuck ()
put' = do
  Context p v <- get
  let c = chr $ v ! p
  tell . T.singleton $ c
  pure ()

get' :: Brainfuck ()
get' = do
  Context p v <- get
  c <- liftIO $ getChar
  let v' = v // [(p, ord c)]
  put $ Context p v'

conditional :: [Brainfuck ()] -> Brainfuck ()
conditional bfs = do
  Context p v <- get
  when (v ! p /= 0) $ sequenceA_ bfs *> conditional bfs
