module Brainfuck.Parser (parseBrainfuck) where

import Data.Functor (($>))
import qualified Data.Text.Lazy as T
import Data.Void (Void)
import qualified Text.Megaparsec as M
import Text.Megaparsec ((<|>))
import qualified Text.Megaparsec.Char as M

import Brainfuck (BF(..))

type Parser = M.Parsec Void T.Text

parseBrainfuck :: T.Text -> Maybe [BF]
parseBrainfuck = M.parseMaybe source

source :: Parser [BF]
source = M.many bf <* M.eof

bf :: Parser BF
bf =  M.try conditional
  <|> M.try moveRight
  <|> M.try moveLeft
  <|> M.try increment
  <|> M.try decrement
  <|> M.try put
  <|> M.try get

moveRight :: Parser BF
moveRight = M.char '>' $> MoveRight

moveLeft :: Parser BF
moveLeft = M.char '<' $> MoveLeft

increment :: Parser BF
increment = M.char '+' $> Increment

decrement :: Parser BF
decrement = M.char '-' $> Decrement

put :: Parser BF
put = M.char '.' $> Put

get :: Parser BF
get = M.char ',' $> Get

conditional :: Parser BF
conditional = Conditional <$> M.between (M.char '[') (M.char ']') (M.many bf)
