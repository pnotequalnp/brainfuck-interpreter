module Main (main) where

import Data.Text.Lazy.IO as T
import System.Environment (getArgs)

import Brainfuck (evalBF)
import Brainfuck.Parser (parseBrainfuck)

main :: IO ()
main = do
  [fp] <- getArgs
  Just bf <- parseBrainfuck <$> T.readFile fp
  (_, output, _, _) <- evalBF bf
  T.putStr output
